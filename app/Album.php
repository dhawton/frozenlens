<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Album
    extends Model
{
    protected $table = "albums";
    protected $fillable = ['name','description','cover_id'];
    public function photos() {
        return $this->hasMany('App\Photo');
    }
    public function cover() {
        return $this->belongsTo('App\Photo', 'cover_id', 'id');
    }
}