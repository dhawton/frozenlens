<?php
namespace App\Http\Controllers;

use App\User;
use Socialite;
use \Auth;

class AuthController
    extends Controller
{
    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleProviderCallback()
    {
        try {
            $user = Socialite::driver('google')->user();
        } catch (Exception $e) {
            return Redirect::to('/login');
        }

        $authUser = $this->findOrCreateUser($user);
        \Auth::login($authUser, true);

        return redirect()->intended('/');
    }

    private function findOrCreateUser($user)
    {
        if  ($au = User::where('email', $user->email)->first()) {
            return $au;
        }

        return User::create([
            'name' => $user->name,
            'email' => $user->email,
            'avatar' => $user->avatar,
            'role' => 'user'
        ]);
    }
}