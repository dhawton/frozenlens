<?php

namespace App\Http\Controllers;

use App\Photo;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use App\Album;

class GalleryController extends Controller {
    public function getIndex() {
        $albums = Album::all();
        return view('gallery.index', ['albums' => $albums]);
    }
    public function getAlbum($id) {
        $album = Album::find($id);
        if (!$album) return redirect('/gallery')->with('error',"Album not found");

        return view('gallery.album', ['album' => $album]);
    }
    public function postAlbum(Request $request, $id) {
        if (!$request->ajax()) abort(403);
        if (!\Auth::check() || \Auth::user()->role != "admin") abort(401);

        $album = Album::find($id);
        if (!$album) abort(404);
        if ($request->input('name')) $album->name = $request->input('name');
        if ($request->input('desc')) $album->description = $request->input('desc');
        $album->save();
    }
    public function deleteAlbum(Request $request, $id) {
        if (!$request->ajax()) abort(401);
        if (!\Auth::check() || \Auth::user()->role != "admin") abort(401);

        $album = Album::find($id);
        if (!$album) abort(404);

        foreach ($album->photos as $photo) {
            $url = parse_url($photo->url);
            unlink(public_path() . $url['path']);
            $url = parse_url($photo->thumbnail_url);
            unlink(public_path() . $url['path']);
            $photo->delete();
        }

        $album->delete();
    }
    public function putGallery(Request $request)
    {
        if (!$request->ajax()) abort(403);
        if (!\Auth::check() || \Auth::user()->role != "admin") abort(401);

        parse_str(file_get_contents("php://input"),$vars);

        $album = new Album();
        $album->name = $vars['name'];
        $album->description = "";
        $album->cover_id = 1;
        $album->save();
    }

    public function getAlbumUpload($id)
    {
        if (!\Auth::check() || \Auth::user()->role != "admin") abort(401);

        return view('gallery.upload', ['id' => $id]);
    }

    public function postAlbumUpload(Request $request, $id)
    {
        $album = Album::find($id);
        if (!$album) return redirect('/gallery')->with('error', 'Invalid album');

        $img = new Photo();
        $img->name = $request->input('photoName', '');
        $img->description = $request->input('photoDesc', '');
        $img->album_id = $id;
        //$img->save();
        $ts = md5(microtime(true));
        $file = request()->file('photoFile');
        $filename = $ts . "." . $file->getClientOriginalExtension();
        request()->file('photoFile')->storeAs('photos', $filename);
        exec("convert " . public_path() . "/assets/gallery/photos/$filename -resize \"300x300^\" -gravity center -crop 300x300+0+0 +repage " . public_path() . "/assets/gallery/thumb/$filename");

        $img->thumbnail_url = "/assets/gallery/thumb/$ts." . $file->getClientOriginalExtension();
        $img->url = "/assets/gallery/photos/$ts." . $file->getClientOriginalExtension();
        $img->save();

        return redirect("/gallery/$id")->with('success','Image uploaded');
    }

    public function getAlbumPhoto($aid, $id) {
        $photo = Photo::find($id);
        if (!$photo) return redirect('/gallery')->with('error','Photo not found');

        return view('gallery.photo', ['photo' => $photo]);
    }

    public function postAlbumPhoto(Request $request, $aid, $id) {
        if (!$request->ajax()) abort(403);
        if (!\Auth::check() || \Auth::user()->role != "admin") abort(401);

        $photo = Photo::find($id);
        if (!$photo) abort(404);
        if ($request->input('name')) $photo->name = $request->input('name');
        if ($request->input('desc')) $photo->description = $request->input('desc');
        if ($request->input('cover')) {
            $album = Album::find($photo->album_id);
            $album->cover_id = $photo->id;
            $album->save();
        }
        $photo->save();
    }
    public function deleteAlbumPhoto(Request $request, $aid, $id) {
        if (!$request->ajax()) abort(403);
        if (!\Auth::check() || \Auth::user()->role != "admin") abort(401);

        $photo = Photo::find($id);
        if (!$photo) abort(404);

        $url = parse_url($photo->url);
        unlink(public_path() . $url['path']);
        $url = parse_url($photo->thumbnail_url);
        unlink(public_path() . $url['path']);

        $photo->delete();
    }
}