@extends('layout')

@section('content')
    @if(\Auth::check() && \Auth::user()->role == "admin")
        <div class="row">
            <div class="col-sm-12">
                <button class="btn btn-md btn-primary" id="btnUpload"><span class="fa fa-upload"></span> Upload</button>
                <button class="btn btn-md btn-warning" id="btnEditName"><span class="fa fa-pencil-square-o"></span>
                    Rename
                </button>
                <button class="btn btn-md btn-success" id="btnEditDesc"><span class="fa fa-pencil-square-o"></span>
                    Description
                </button>
                <button class="btn btn-md btn-danger" id="btnDelete"><span class="fa fa-trash-o"></span> Delete</button>
            </div>
        </div><br>
    @endif
    <div class="row">
        <div class="col-md-12">
            <h4><a href="/gallery">Gallery</a> | {{$album->name}}</h4>
        </div>
    </div>
    @if(count($album->photos) == 0)
        <div class="row">
            <div class="col-sm-12"><i>No photos in album</i></div>
        </div>
    @else
        <div class="row container-grid">
            @foreach($album->photos as $photo)
                <div class="nf-item col-md-3 col-sm-6 col-xs-12 mb-30">
                    <div class="item-box">
                        <div class="gallery-album">
                            <a href="/gallery/{{$album->id}}/{{$photo->id}}">
                                <img src="{{$photo->thumbnail_url}}" alt="{{$photo->name}}" class="item-container">
                                <div class="item-mask">
                                </div>
                            </a>
                        </div>
                        <div class="gallery-album-info">
                            <a href="/gallery/{{$album->id}}/{{$photo->id}}">
                                <h4 class="gallery-album-name">{{$photo->name}}</h4>
                            </a>
                        </div>
                    </div>
                </div>
                @if(($loop->iteration % 4) == 0 && !$loop->last)
        </div>
        <div class="row container-grid">
            @endif
            @endforeach
        </div>
    @endif
@endsection

@if(\Auth::check() && \Auth::user()->role == "admin")
@section('javascript')
    <script type="text/javascript">
        $('#btnUpload').click(function () {
            window.location = "/gallery/{{$album->id}}/upload";
        });
        $('#btnEditName').click(function () {
            bootbox.prompt({
                title: "Enter new name",
                value: "{{$album->name}}",
                callback: function (r) {
                    if (r === null) return false;

                    $.ajax({
                        url: "/gallery/{{$album->id}}",
                        method: "POST",
                        data: {name: r}
                    }).done(function () {
                        location.reload();
                    });
                }
            });
        });
        $('#btnEditDesc').click(function () {
            bootbox.prompt({
                title: "Enter new description",
                value: "{{$album->desc}}",
                inputType: "textarea",
                callback: function (r) {
                    if (r === null) return false;

                    $.ajax({
                        url: "/gallery/{{$album->id}}",
                        method: "POST",
                        data: {desc: r}
                    }).done(function () {
                        location.reload();
                    });
                }
            });
        });
        $('#btnDelete').click(function () {
            bootbox.confirm({
                size: "small",
                message: "Are you sure?",
                callback: function (r) {
                    if (r === null) return false;

                    $.ajax({
                        url: "/gallery/{{$album->id}}",
                        method: "DELETE",
                    }).done(function () {
                        location.href="/gallery";
                    });
                }
            });
        });
    </script>
@endsection
@endif