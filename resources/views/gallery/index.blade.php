@extends('layout')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h4>Gallery</h4>
        </div>
    </div>
@if(count($albums) == 0)
    <div class="row">
    <div class="col-sm-12"><i>No albums currently</i></div>
    </div>
@else
    <div class="row container-grid">
        @foreach($albums as $album)
        <div class="nf-item col-md-3 col-sm-6 col-xs-12 mb-30">
            <div class="item-box">
                <div class="gallery-album">
                <a href="/gallery/{{$album->id}}">
                    <img src="{{$album->cover->thumbnail_url}}" alt="{{$album->name}}" class="item-container">
                    <div class="item-mask">
                    </div>
                </a>
                </div>
                <div class="gallery-album-info">
                    <a href="/gallery/{{$album->id}}">
                        <h4 class="gallery-album-name">{{$album->name}}</h4>
                    </a>
                </div>
            </div>
        </div>
        @if(($loop->iteration % 4) == 0 && !$loop->last)
    </div>
    <div class="row container-grid">
        @endif
        @endforeach
    </div>
@endif
@if(\Auth::check() && \Auth::user()->role == "admin")
    <div class="row">
        <div class="col-sm-12">
            <button class="btn btn-md btn-primary" id="btnAddAlbum">Create Album</button>
        </div>
    </div>
@endif
@endsection

@section('javascript')
    <script type="text/javascript">
        $('#btnAddAlbum').click(function() {
            bootbox.prompt("Name of Album:", function(result) {
                if (result === null) return false;
                $.ajax({
                    url: '/gallery',
                    method: 'PUT',
                    data: {name: result},
                }).done(function() {
                    location.reload();
                });
            })
        });
    </script>
@endsection