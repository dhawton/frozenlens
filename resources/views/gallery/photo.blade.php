@extends('layout')

@section('content')
    @if(\Auth::check() && \Auth::user()->role == "admin")
        <div class="row mb-30 mb-xs-30">
            <div class="col-sm-12">
                <button class="btn btn-md btn-primary" id="btnSetCover"><span class="fa fa-star-o"></span> Album Cover</button>
                <button class="btn btn-md btn-warning" id="btnEditName"><span class="fa fa-pencil-square-o"></span>
                    Rename
                </button>
                <button class="btn btn-md btn-success" id="btnEditDesc"><span class="fa fa-pencil-square-o"></span>
                    Description
                </button>
                <button class="btn btn-md btn-danger" id="btnDelete"><span class="fa fa-trash-o"></span> Delete</button>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <h4><a href="/gallery">Gallery</a> | <a href="/gallery/{{$photo->album_id}}">{{$photo->album->name}}</a> | {{$photo->name}}</h4>
        </div>
    </div>
    @if($photo->description)
    <div class="row">
        <div class="col-md-12">
            <p class="lead">
                {{$photo->description}}
            </p>
        </div>
    </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <img src="{{$photo->url}}" alt="{{$photo->name}}">
        </div>
    </div>
@endsection

@if(\Auth::check() && \Auth::user()->role == "admin")
@section('javascript')
    <script type="text/javascript">
        $('#btnSetCover').click(function() {
            $.ajax({
                url: '/gallery/{{$photo->album_id}}/{{$photo->id}}',
                method: 'POST',
                data: { cover: 1 },
            }).done(function() {
                bootbox.alert("Set as album cover");
            });
        });
        $('#btnEditName').click(function () {
            bootbox.prompt({
                title: "Enter new name",
                value: "{{$photo->name}}",
                callback: function (r) {
                    if (r === null) return false;

                    $.ajax({
                        url: "/gallery/{{$photo->album_id}}/{{$photo->id}}",
                        method: "POST",
                        data: {name: r}
                    }).done(function () {
                        location.reload();
                    });
                }
            });
        });
        $('#btnEditDesc').click(function () {
            bootbox.prompt({
                title: "Enter new description",
                value: "{{$photo->description}}",
                inputType: "textarea",
                callback: function (r) {
                    if (r === null) return false;

                    $.ajax({
                        url: "/gallery/{{$photo->album_id}}/{{$photo->id}}",
                        method: "POST",
                        data: {desc: r}
                    }).done(function () {
                        location.reload();
                    });
                }
            });
        });
        $('#btnDelete').click(function () {
            bootbox.confirm({
                size: "small",
                message: "Are you sure?",
                callback: function (r) {
                    if (r === null) return false;

                    $.ajax({
                        url: "/gallery/{{$photo->album_id}}/{{$photo->id}}",
                        method: "DELETE",
                    }).done(function () {
                        location.href="/gallery/{{$photo->album_id}}";
                    });
                }
            });
        });
    </script>
@endsection
@endif