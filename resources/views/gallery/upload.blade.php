@extends('layout')

@section('content')
    <form action="/gallery/{{$id}}/upload" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group">
            <label for="photoName">Name</label>
            <input type="text" class="form-control" id="photoName" name="photoName" placeholder="Name">
        </div>
        <div class="form-group">
            <label for="photoDesc">Description</label>
            <textarea id="photoDesc" class="form-control" name="photoDesc" rows="3"></textarea>
        </div>
        <div class="form-group">
            <label for="photoFile">Image File</label>
            <input type="file" id="photoFile" name="photoFile">
        </div>
        <button type="submit" class="btn btn-md btn-color">Upload</button>
    </form>
@endsection