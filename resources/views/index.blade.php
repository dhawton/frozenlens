<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>FrozenLens</title>
    <meta name="description" content="Daniel A. Hawton photography">
    <meta name="keywords" content="Daniel Hawton photography frozen lens frozenlens frozenphotos">
    <meta name="author" content="Daniel A. Hawton">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible' content='IE=edge,chrome=1'>
    <![endif]-->

    <link href="/assets/css/style.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/ionicons.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/plugin/sidebar-menu.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/plugin/animate.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/jquery-ui.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/rs-settings.css" rel="stylesheet" type="text/css">
</head>
<body>
    <section id="preloader">
        <div class="loader" id="loader">
            <div class="loader-img"></div>
        </div>
    </section>

    <section id="pushmenu-right" class="pushmenu pushmenu-right side-menu">
        <a id="menu-sidebar-close-icon" class="menu-close"><i class="ion ion-android-close"></i></a>
        @if(!\Auth::check())
        <h5 class="white">Sign In</h5>
        <div class="sign-in">
            <button class="btn btn-md btn-danger form-full" id="btnSignin">With Google</button>
        </div>
        @else
        <h5 class="white">Hello!</h5>
        <div class="sign-in">
            <button class="btn btn-md btn-danger form-full" id="btnSignout">Logout</button>
        </div>
        @endif
        <ul>
            <li><a href="/">Home</a></li>
            <li><a href="/gallery">Gallery</a></li>
            <li><a href="/projects">Projects</a></li>
            <li><a href="http://www.danielhawton.com">Resume/Projects</a></li>
            @if(\Auth::check() and \Auth::user()->role == "admin")
            <li><a href="/admin/gallery/album">Album Mgr</a></li>
            @endif
        </ul>
    </section>

    <div class="wrapper">
        <header id="header" class="header header-gray">
            <div class="container header-inner">
                <div class="logo">
                    <a href="/">
                        <img class="logo-light" src="/assets/img/logo-white.png" alt="FrozenLens">
                        <img class="logo-dark" src="/assets/img/logo-color.png" alt="FrozenLens">
                    </a>
                </div>

                <div class="side-menu-btn">
                    <ul>
                        <li><a id="menu-sidebar-list-icon" class="nav-bar-icon"><span></span></a></li>
                    </ul>
                </div>

                <div class="nav-mobile nav-bar-icon">
                    <span></span>
                </div>

                <div class="nav-menu">
                    <ul class="nav-menu-inner">
                        <li><a href="/">Home</a></li>
                        <li><a href="/gallery">Gallery</a></li>
                        <li><a href="http://www.danielhawton.com">Resume/Projects</a></li>
                    </ul>
                </div>
            </div>
        </header>

        <section id="intro" class="intro">
            <div class="tp-banner-container">
                <div class="intro-RevSlider text-center">
                    <ul>
                        <li class="dark-color" data-transition="fade" data-slotamount="20" data-masterspeed="1000" data-slide="light-slide">
                            <img src="/assets/img/sliders/10.jpg" alt="Slide 10" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no repeat">
                        </li>
                        <li class="dark-color" data-transition="fade" data-slotamount="20" data-masterspeed="1000" data-slide="light-slide">
                            <img src="/assets/img/sliders/3.jpg" alt="Slide 3" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no repeat">
                        </li>
                        <li class="dark-color" data-transition="fade" data-slotamount="20" data-masterspeed="1000" data-slide="light-slide">
                            <img src="/assets/img/sliders/6.jpg" alt="Slide 6" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no repeat">
                        </li>
                        <li class="light-color" data-transition="fade" data-slotamount="20" data-masterspeed="1000" data-slide="dark-slide">
                            <img src="/assets/img/sliders/9.jpg" alt="Slide 9" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no repeat">
                        </li>
                        <li class="light-color" data-transition="fade" data-slotamount="20" data-masterspeed="1000" data-slide="dark-slide">
                            <img src="/assets/img/sliders/11.jpg" alt="Slide 11" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no repeat">
                        </li>
                    </ul>
                </div>
            </div>
        </section>
    </div>

    <script src="/assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="/assets/js/plugin/jquery.easing.js" type="text/javascript"></script>
    <script src="/assets/js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="/assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/assets/js/plugin/jquery.viewportchecker.js" type="text/javascript"></script>
    <script src="/assets/js/plugin/jquery.stellar.min.js" type="text/javascript"></script>
    <script src="/assets/js/plugin/wow.min.js" type="text/javascript"></script>
    <script src="/assets/js/plugin/jquery.colorbox-min.js" type="text/javascript"></script>
    <script src="/assets/js/plugin/owl.carousel.min.js" type="text/javascript"></script>
    <script src="/assets/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
    <script src="/assets/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
    <script src="/assets/js/plugin/isotope.pkgd.min.js" type="text/javascript"></script>
    <script src="/assets/js/plugin/masonry.pkgd.min.js" type="text/javascript"></script>
    <script src="/assets/js/plugin/imagesloaded.pkgd.min.js" type="text/javascript"></script>
    <script src="/assets/js/plugin/sidebar-menu.js" type="text/javascript"></script>
    <script src="/assets/js/plugin/jquery.fs.tipper.min.js" type="text/javascript"></script>
    <script src="/assets/js/plugin/mediaelement-and-player.min.js"></script>
    <script src="/assets/js/revolution-slider.js" type="text/javascript"></script>
    <script src="/assets/js/theme.js" type="text/javascript"></script>
    <script src="/assets/js/navigation.js" type="text/javascript"></script>
<script type="text/javascript">
    $('#btnSignin').click(function() { window.location = "https://www.frozenlens.org/login"; })
    $('#btnSignout').click(function() { window.location = "https://www.frozenlens.org/logout"; })
</script>
</body>
</html>
