<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/gallery', 'GalleryController@getIndex');
Route::put('/gallery', 'GalleryController@putGallery');

Route::get('/gallery/{id}', 'GalleryController@getAlbum');
Route::post('/gallery/{id}', 'GalleryController@postAlbum');
Route::delete('/gallery/{id}', 'GalleryController@deleteAlbum');
Route::get('/gallery/{id}/upload', 'GalleryController@getAlbumUpload');
Route::post('/gallery/{id}/upload', 'GalleryController@postAlbumUpload');

Route::get('/gallery/{aid}/{id}', 'GalleryController@getAlbumPhoto');
Route::post('/gallery/{aid}/{id}', 'GalleryController@postAlbumPhoto');
Route::delete('/gallery/{aid}/{id}', 'GalleryController@deleteAlbumPhoto');

Route::get('/test/album/{id}', function($id) {
    var_dump(\App\Album::find($id));
    var_dump(\App\Album::find($id)->cover);
});

Route::post('/git', function() {
    system("cd /home/frozenlens/frozenlens && git pull origin");
});

Route::get('/login', 'AuthController@redirectToProvider');
Route::get('/login/google', 'AuthController@handleProviderCallback');
Route::get('/logout', function() {
    \Auth::logout();
    return redirect('/');
});